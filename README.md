# Paradox API for IP150

The Paradox API allow you to : 
 - Read/write areas state of you Paradox system
 - Toggle ip150 ios

A web server is exposed an you can use REST operation to manage our Alarm.

**The Paradox API needs the IP150 module to connect to your Paradox system.**

## Configuration
```
{
  "port": 3000,
  "logger": "Info",
  "ipModule": {
    "url": "http://localhost:3001",
    "username": "1234",
    "password": "secret"
  },
  "api": {
    "users": [
      {
        "username": "paradox",
        "password": "api"
      }
    ]
  }
}
```

## Running

## With Docker (the recommended way)

Once started: 
 - Go to _http://localhost:3000/_ to manage your module
 - Go to _http://localhost:3000/api/_ to see all available services

### Command line

    sudo docker run -d \
    -v /local/config.json:/config.json \
    -p 3000:3000 \
    -e UID=1001 \
    -e GID=1001 \
    --name paradox-api \
    calfater/paradox-api

### With crane (the royal recommended way)

```crane
  paradox-api:  
    image: calfater/paradox-api:latest  
    detach: true  
    env:
      - UID=1001
      - GID=1001
    ports:  
      - "3000:3000"
    volumes:
      - paradox-api/config.json:/config.json
```

## Without Docker 

Once started: 
 - Go to _http://localhost:3000/_ to manage your module
 - Go to _http://localhost:3000/api/_ to see all available services

**Config file is located in `~/.paradox-api.json`**

Install node and npm `sudo apt-get install nvm && nvm install 12.4.0`

### API

`cd server && npm install && npm run build && npm run dev`

### Web app

`cd web && npm install && npm run build && npm run dev`

## Testing  / debugging

### View logs

`sudo docker logs -f paradox-api`

### Connects on container

`sudo docker exec paradox-api bash`

### Run mock

`cd ip150mock && npm install && npm start`

## Building

### Docker
    docker build --build-arg U=paradoxapi -t calfater/paradox-api:latest .