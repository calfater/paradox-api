import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AreasComponent} from "./controllers/areas/areas.component";
import {IosComponent} from "./controllers/ios/ios.component";
import {EventsComponent} from "./controllers/events/events.component";


const routes: Routes = [
  {path: '', redirectTo: '/areas', pathMatch: 'full'},
  {path: 'areas', component: AreasComponent, runGuardsAndResolvers: "always"},
  {path: 'ios', component: IosComponent},
  {path: 'events', component: EventsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
