import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AreasComponent} from './controllers/areas/areas.component';
import {HttpClientModule} from "@angular/common/http";
import {SpinnerComponent} from './components/spinner/spinner.component';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {AreaBtnComponent} from './components/area-btn/area-btn.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {IosComponent} from './controllers/ios/ios.component';
import {ButtonComponent} from './components/button/button.component';
import {EventsComponent} from './controllers/events/events.component';
import {ButtonReloadComponent} from './components/button-reload/button-reload.component';
import { PageTitleComponent } from './components/page-title/page-title.component';

@NgModule({
  declarations: [
    AppComponent,
    AreasComponent,
    SpinnerComponent,
    AreaBtnComponent,
    IosComponent,
    ButtonComponent,
    EventsComponent,
    ButtonReloadComponent,
    PageTitleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FontAwesomeModule,
    NgbModule
  ],
  providers: [
    // {provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    // library.addIcons(faSpinner);
    // library.addIcons(faBug);
  }

}
