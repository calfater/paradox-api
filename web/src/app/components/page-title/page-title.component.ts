import {Component, Input, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.scss']
})
export class PageTitleComponent implements OnInit {

  @Input()
  private title: string;

  constructor(private titleService: Title) {
  }

  ngOnInit() {
    console.log("Set title :  " + this.title);
    this.titleService.setTitle(this.title)
  }

}
