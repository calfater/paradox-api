import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-area-btn',
  templateUrl: './area-btn.component.html',
  styleUrls: ['./area-btn.component.scss']
})
export class AreaBtnComponent implements OnInit {

  @Input()
  private ngModel: any;

  constructor() { }

  ngOnInit() {
  }

}
