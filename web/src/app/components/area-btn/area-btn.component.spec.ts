import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaBtnComponent } from './area-btn.component';

describe('AreaBtnComponent', () => {
  let component: AreaBtnComponent;
  let fixture: ComponentFixture<AreaBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
