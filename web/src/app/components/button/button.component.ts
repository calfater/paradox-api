import {Component, Input, OnInit} from '@angular/core';
import {faCheckCircle, faSpinner, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {Observable} from "rxjs";

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  public icon: any = null;
  private iconClass: string = "";
  private iconSpin: boolean = false;
  private disabled: boolean = false;

  private _action;

  @Input("type")
  private type: string;

  @Input()
  set action(action: any) {
    if ('function' !== typeof (action)) {
      throw Error("action must be a function");
    }
    this._action = action;
  };

  constructor() {
  }

  ngOnInit() {
  }

  getClass() {
    let c = "btn ";
    switch (this.type) {
      case "success":
        c += "btn-outline-success";
        break;
      case "danger":
        c += "btn-outline-danger";
        break;
      case "light":
        c += "btn-light";
        break;
      default:
        c += "primary";
    }
    return c;
  }

  differedResetIcon() {
    setTimeout(() => {
      this.icon = null;
    }, 3000);
  };

  doClick() {
    this.disabled = true;
    this.icon = faSpinner;
    this.iconClass = "text-dark";
    this.iconSpin = true;
    let actionResult: Observable<void> = this._action();
    if (actionResult && actionResult['subscribe']) {
      actionResult.subscribe(
        () => {
        },
        () => {
          this.icon = faTimesCircle;
          this.iconClass = "text-danger";
          this.iconSpin = false;
          this.disabled = false;
          this.differedResetIcon();
        },
        () => {
          this.icon = faCheckCircle;
          this.iconClass = "text-success";
          this.iconSpin = false;
          this.disabled = false;
          this.differedResetIcon();
        }
      );
    }
  }
}
