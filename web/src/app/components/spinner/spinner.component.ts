import {Component, Input, OnInit} from '@angular/core';
import {SpinnerError} from "./spinner-error";

// import {faSpinner} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input()
  private ngModel: any;
  @Input()
  private spinnerError: SpinnerError = null;

  constructor() {
  }

  ngOnInit() {
    // this.x = faSpinner;
    // console.log(this.x)
  }

}
