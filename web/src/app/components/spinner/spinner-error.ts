export class SpinnerError {
  public hasError: boolean;
  public errorMessage: string;
}
