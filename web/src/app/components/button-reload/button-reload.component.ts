import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";

@Component({
  selector: 'app-button-reload',
  templateUrl: './button-reload.component.html',
  styleUrls: ['./button-reload.component.scss']
})
export class ButtonReloadComponent implements OnInit {

  @Input()
  private action: Observable<any>;
  private inProgress: boolean = false;
  private error: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }

  doClick() {
    this.inProgress = true;
    this.action.subscribe(() => {
    }, () => {
      this.error = true;
      this.inProgress = false;
    }, () => {
      this.error = false;
      this.inProgress = false;
    })
  }

}
