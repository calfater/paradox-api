import { Component, OnInit } from '@angular/core';
import {SpinnerError} from "../../components/spinner/spinner-error";
import {CommunicationService} from "../../services/communication/communication.service";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  public events: Array<any>;
  public eventsError: SpinnerError = new SpinnerError();

  constructor(private communicationService: CommunicationService) {
  }

  ngOnInit() {
    this.updateEvents();
  }

  private updateEvents() {
    this.communicationService.getEvents().subscribe(data => {
      this.eventsError.hasError = false;
      this.events = data;
    }, error => {
      this.eventsError.hasError = true;
      this.eventsError.errorMessage = error.message;
      console.error(error);
    })
  }

}
