import {Component, OnInit} from '@angular/core';
import {SpinnerError} from "../../components/spinner/spinner-error";
import {CommunicationService} from "../../services/communication/communication.service";
import {Observable, of} from "rxjs";
import {IoCfgDto} from "../../../../../server/src/services/ip150/dto/io-cfg-dto";

@Component({
  selector: 'app-ios',
  templateUrl: './ios.component.html',
  styleUrls: ['./ios.component.scss']
})
export class IosComponent implements OnInit {

  public ios: Array<IoCfgDto>;
  public iosError: SpinnerError = new SpinnerError();

  public toggle: any = (id) =>
    (): Observable<void> =>
      this.communicationService.toggleIo(id);

  constructor(private communicationService: CommunicationService) {
  }

  setArmingMode(io) {
    this.communicationService.toggleIo(io).subscribe(() => {
      this.updateIos();
    }, () => {
    });
  }

  ngOnInit() {
    this.updateIos();
  }

  private updateIos() {
    this.communicationService.getIos().subscribe(data => {
      this.iosError.hasError = false;
      this.ios = data;
    }, error => {
      this.iosError.hasError = true;
      this.iosError.errorMessage = error.message;
      console.error(error);
    })
  }

}
