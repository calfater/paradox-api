import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommunicationService} from "../../services/communication/communication.service";
import {SpinnerError} from "../../components/spinner/spinner-error";
import {NavigationEnd, Router} from "@angular/router";
import {Observable, Subscription} from "rxjs";

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit, OnDestroy {

  public areas: Array<any>;
  public areasError: SpinnerError = new SpinnerError();
  private navigationSubscription: Subscription = null;

  constructor(private router: Router, private communicationService: CommunicationService) {
  }

  ngOnInit() {
    this.navSubscribe();
    this.updateAreas();
  }

  ngOnDestroy() {
    this.navUnSubscritbe();
  }

  setArmingMode(area, mode) {
    this.communicationService.setAreas(area, mode).subscribe({
      complete: () => this.updateAreas()
    });
  }

  private updateAreas() {
    this.manualUpdateAreas().subscribe();
  }

  manualUpdateAreas(): Observable<any> {
    return new Observable(observer => {
      this.communicationService.getAreas().subscribe(data => {
        this.areasError.hasError = false;
        this.areas = data;
        // this.scheduleAnUpdate();
      }, error => {
        this.areasError.hasError = true;
        this.areasError.errorMessage = error.message;
        console.error(error);
        // this.scheduleAnUpdate();
        observer.error(error);
      }, () => {
        observer.complete();
      })
    });
  }

  // private scheduleAnUpdate() {
  //   if (null !== this.navigationSubscription) {
  //     interval(3000).pipe(takeWhile(() => {
  //       if (null !== this.navigationSubscription) {
  //         this.updateAreas.bind(this)();
  //       }
  //       return null;
  //     })).subscribe();
  //   }
  // }

  private navSubscribe() {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.areas = null;
        this.updateAreas();
      }
    })
  }

  private navUnSubscritbe() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
      this.navigationSubscription = null;
    }
  }

}
