import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  constructor(private http: HttpClient) {
  }

  getAreas() {
    return this.http.get<Array<any>>("/api/areas");
  }

  setAreas(area: number, mode: string) {
    return this.http.post<Array<any>>(`/api/areas/${area}`, {armingMode: mode});
  }

  getIos() {
    return this.http.get<Array<any>>("/api/ios");
  }

  toggleIo(io: number) {
    return this.http.post<any>(`/api/ios/${io}`, {});
  }

  getEvents() {
    return this.http.get<any>(`/api/events`);
  }

}
