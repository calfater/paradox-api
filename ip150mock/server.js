const http = require('http');
const fs = require('fs');

let app = http.createServer((req, res) => {
    let url = req.url;

    let fileUrl = "html/" + url.slice(1).replace(/\//, "/")
        .replace(/\.html\?area=.*/, "-set-area.html")
        .replace(/\.html\?msgid=2.*/, "-toggle-io.html")
        .replace(/\.html\?u=.*/, ".html");

    console.log(url + " => " + fileUrl);

    setTimeout(() => {
        try {
            let data = fs.readFileSync(fileUrl);
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(data);
        } catch (e) {
            console.error(e.message);
            res.writeHead(404, {'Content-Type': 'text/html'});
            res.end(`${fileUrl} not found`);
        }
    }, 500)
});

app.listen(3001, '127.0.0.1');
console.log('Node server running on port 3001');