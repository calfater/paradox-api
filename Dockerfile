FROM node:12

ARG U=paradox
ARG G=$U

ENV U=$U \
    G=$G \
    WD=/$U \
    PUID=1000 \
    PGID=1000

WORKDIR $WD
ENTRYPOINT ["./docker/start.sh"]
EXPOSE 3000

# RUN echo " User : $U\n Group : $G\nWorking dir : $WD"

# Build project
COPY docker docker
COPY server server
COPY web web

RUN apt-get update && \
    apt-get -y install sudo

RUN cd server; \
    npm install; \
    npm run build; \
    cd ..; \
    cd web; \
    npm install; \
    npm run build; \
    cd ..; \
    cp -rfv web/dist/web server/dist/webapp

RUN cd docker;\
    chmod +x *.sh

RUN ln -s /config.json .paradox-api.json
