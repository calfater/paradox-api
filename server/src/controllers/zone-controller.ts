import {Controller, Get} from "@tsed/common";
import {ZoneService} from "../services/zone/zone-service";

@Controller("/zones")
export class ZoneController {

    constructor(private zoneService: ZoneService) {
    }

    @Get("")
    getZones() {
        return this.zoneService.getZonesState();
    };


}