import {Controller, Get} from "@tsed/common";
import {EventService} from "../services/event/event-service";

@Controller("/events")
export class EventController {

    constructor(private eventService: EventService) {
    }

    @Get("")
    public getEvents() {
        return this.eventService.getEvents();
    }

}