import {Controller, Get} from "@tsed/common";
import {StatusLiveService} from "../services/status-live/status-live-service";

@Controller("/status-live")
export class StatusLiveController {

    constructor(private statusLiveService: StatusLiveService) {
    }

    @Get()
    getStatusLive() {
        return this.statusLiveService.getStatusLive();
    }
}