import {Controller, Get} from "@tsed/common";
import {Ip150Service} from "../services/ip150/ip150-service";

@Controller("/tests")
export class TestController {

    constructor(private ip150Service: Ip150Service) {
    }

    @Get("")
    public getTests() {
        return this.ip150Service.authenticate()
            .then(() => this.ip150Service.ioStatus())
            .finally(this.ip150Service.logout);
    }
}