import {Controller, Get, PathParams, Post} from "@tsed/common";
import {IoService} from "../services/io/io-service";

@Controller("/ios")
export class IosController {

    constructor(private ioService: IoService) {
    }

    @Get("")
    public getIos() {
        return this.ioService.getIos();
    }

    @Get("/:id")
    public getIo(@PathParams("id") id: number) {
        return this.ioService.getIo(id);
    }

    @Post("/:id")
    public setIo(@PathParams("id") id: number/*, @BodyParams("active") active: IoActive*/)  {
        return this.ioService.toggleIo(id);
    }
}