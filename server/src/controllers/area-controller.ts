import {BodyParams, Controller, Get, PathParams, Post} from "@tsed/common";
import {AreaService} from "../services/area/area-service";
import {ArmingMode} from "../services/ip150/enums/arming-mode";
import {Area} from "../services/ip150/enums/area";
import {NotFound} from "ts-httpexceptions";

@Controller("/areas")
export class AreaController {

    constructor(private areaService: AreaService) {
    }

    @Get("")
    public getAreas() {
        return this.areaService.getAreas();
    }

    @Get("/:area")
    public getAreasId(@PathParams("area") area: Area, @PathParams("area") areaAsNumber: number) {
        this.checkIfAreaExists(area, areaAsNumber);
        return this.areaService.getArea(area);
    };

    @Post("/:area")
    public postAreasId(
        @PathParams("area")  area: Area, @PathParams("area") areaAsNumber: number,
        @BodyParams("armingMode") armingMode: ArmingMode) {
        this.checkIfAreaExists(area, areaAsNumber);
        // console.log(area, armingMode);
        return this.areaService.setAreaArmingMode(area, armingMode);
    };

    private checkIfAreaExists(area: Area, areaAsNumber: number) {
        if (!area) {
            throw new NotFound(`Area '${areaAsNumber}' not found !`);
        }
    }

}