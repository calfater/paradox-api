import {Converter, IConverter, IDeserializer, ISerializer} from "@tsed/common";
import {SearchableByNameAndCode} from "../services/ip150/enums/searchable-by-name-and-code";

@Converter(SearchableByNameAndCode)
export class SearchableByNameAndCodeConverter implements IConverter {

    deserialize(data: any, target: any, baseType: number, deserializer: IDeserializer): SearchableByNameAndCode {
        return SearchableByNameAndCode.fromName("" + data, target);
    }

    serialize(data: SearchableByNameAndCode, serializer: ISerializer) {
        return data.name;
    }

}