import {URL} from "url";
import http, {ClientRequest, IncomingMessage, RequestOptions} from "http";
import https from "https";
import {AreaStatus} from "./enums/area-status";
import {ArmingMode} from "./enums/arming-mode";
import {StatusLiveDto} from "./dto/status-live-dto";
import {Credentials} from "../ip150-js/dto/credentials";
import {ConfigService} from "../config/config-service";
import {LoggerService} from "../logger/logger-service";
import {Ipp150JsService} from "../ip150-js/ip150-js-service";
import {Area} from "./enums/area";
import {AreaStatusDto} from "./dto/area-status-dto";
import {ZoneStatusDto} from "./dto/zone-status-dto";
import {ZoneStatus} from "./enums/zone-status";
import {AlarmeStatusDto} from "./dto/alarme-status-dto";
import {TroubleStatusDto} from "./dto/trouble-status-dto";
import {Service} from "@tsed/di";
import {LabelsDto} from "./dto/labels-dto";
import {IoCfgDto} from "./dto/io-cfg-dto";
import {IoActive} from "./enums/io-active";
import {EventDto} from "./dto/event-dto";
import {decoratorArgs} from "@tsed/core";

interface Http {
    get(url: string | URL, options: RequestOptions | object, callback?: (res: IncomingMessage) => void): ClientRequest;
}

export class AlreadyAuthenticatedExecption implements Error {
    public name = "AlreadyAuthenticatedExecption";

    constructor(public message: string) {
        this.message = message;
    }
}

class Ip150ServiceAuthentication {
    public date: Date;

    constructor() {
        this.date = new Date();
    }

}

@Service()
export class Ip150Service {

    private logger = new LoggerService(__filename);
    private MAX_READ_ATTEMPTS: number = 3;
    private AUTHENTICATION_ATTEMPT_MAX_DURATION: number = 30;

    private PAGE_LOGIN: string = "/login_page.html";
    private PAGE_DEFAULT: string = "/default.html?u={username}&p={password}";
    private PAGE_LOGOUT: string = "/logout.html";
    private PAGE_LIVESTATUS: string = "/statuslive.html";
    private PAGE_EVENT: string = "/event.html";
    private PAGE_SET_AREA_STATUS: string = "/statuslive.html?area={area}&value={mode}";
    private PAGE_IO: string = "/io_sync.html?msgid=2&num={ioId}&{rnd}";

    private host: string = this.config.config.ipModule.url;
    private connector: Http = this.host.startsWith("https") ? https as Http : http as Http;
    private cacheLabels: LabelsDto = null;
    private authenticationAttempts: Array<Ip150ServiceAuthentication> = new Array<Ip150ServiceAuthentication>();

    constructor(private config: ConfigService, private ip150Js: Ipp150JsService) {
    }

    public authenticate = () => {

        this.authenticationAttempts = this.authenticationAttempts.filter(value => value.date.getTime() + this.AUTHENTICATION_ATTEMPT_MAX_DURATION * 1000 > new Date().getTime());

        this.authenticationAttempts.push(new Ip150ServiceAuthentication());
        this.logger.debug(this.authenticationAttempts);

        return new Promise((resolve, reject) => {
            if (1 == this.authenticationAttempts.length) {
                this.httpCall(this.PAGE_LOGIN)
                    .then((html: string) => new Promise<Credentials>((resolve, reject) => {
                        const session: string = html.split('loginaff("')[1].split('",')[0];
                        const username: string = this.config.config.ipModule.username;
                        const password: string = this.config.config.ipModule.password;
                        const credentials: Credentials = this.ip150Js.loginencrypt(username, password, session);
                        resolve(credentials);
                    }))
                    .then((credentials: Credentials) => this.httpCall(this.PAGE_DEFAULT.replace('{username}', credentials.username).replace('{password}', credentials.password)))
                    .then(resolve)
            } else if (this.authenticationAttempts.length > 1) {
                let msg = "Already authenticated (" + (this.authenticationAttempts.length - 1) + " times) !";
                this.logger.warn(msg);
                throw new AlreadyAuthenticatedExecption(msg)
            } else {
                let msg = "Inconsistent authentication count : " + this.authenticationAttempts.length + " !";
                this.logger.warn(msg);
                throw new AlreadyAuthenticatedExecption(msg)
            }
        });
    };

    // public logout = () => this.httpCall(this.PAGE_LOGOUT).then(() => new Promise(resolve => resolve()));

    public logout = (): Promise<any> => {
        this.authenticationAttempts.pop();
        this.logger.debug(this.authenticationAttempts);
        return new Promise((resolve, reject) => {
            if (0 === this.authenticationAttempts.length) {
                this.httpCall(this.PAGE_LOGOUT).then(() => {
                    resolve();
                })
            } else {
                this.logger.warn("Already authenticated by another process on the Pradox ip module. Keeping existing connexion alive.");
            }
        })
    };

    public statusLive = () => {
        return new Promise<StatusLiveDto>(async (resolve, reject) => {
            let foundAGoodAreaStatus = false;
            let attemptsCount = 0;
            while (attemptsCount < this.MAX_READ_ATTEMPTS && !foundAGoodAreaStatus) {
                ++attemptsCount;

                let data: string = await this.httpCall(this.PAGE_LIVESTATUS);
                if (!data.includes("tbl_useraccess = new Array(")) {
                    reject("Invalid server response");
                    return;
                }

                const areas_machine = JSON.parse('[' + data.split('tbl_useraccess = new Array(')[1].split(')')[0] + ']');
                const zones_machine = JSON.parse('[' + data.split('tbl_statuszone = new Array(')[1].split(')')[0] + ']');
                const alarms_machine = JSON.parse('[' + data.split('tbl_alarmes = new Array(')[1].split(')')[0] + ']');
                const troubles_machine = JSON.parse('[' + data.split('tbl_troubles = new Array(')[1].split(')')[0] + ']');

                let areas: Array<AreaStatusDto> = [];
                let zones: Array<ZoneStatusDto> = [];
                let alarms: Array<AlarmeStatusDto> = [];
                let troubles: Array<TroubleStatusDto> = [];

                for (let idx in areas_machine) {
                    if ("" + areas_machine[idx] != "0") {
                        foundAGoodAreaStatus = true;
                    }
                    areas.push(new AreaStatusDto(Area.fromName(Number(idx) + 1, Area), AreaStatus.fromCode(areas_machine[idx], AreaStatus)));
                }
                for (let idx in zones_machine) {
                    zones.push(new ZoneStatusDto(Number(idx) + 1, ZoneStatus.fromCode(zones_machine[idx], ZoneStatus)));
                }
                for (let idx in alarms_machine) {
                    //TODO: implements me !
                }
                for (let idx in troubles_machine) {
                    //TODO: implements me !
                }

                let allStatus = new StatusLiveDto();
                allStatus.areas = areas;
                allStatus.zones = zones;
                allStatus.alarms = alarms;
                allStatus.troubles = troubles;

                this.logger.debug(JSON.stringify(allStatus.areas));
                this.logger.debug(JSON.stringify(allStatus.zones));
                this.logger.debug(JSON.stringify(allStatus.alarms));
                this.logger.debug(JSON.stringify(allStatus.troubles));

                if (foundAGoodAreaStatus) {
                    resolve(allStatus);
                    return;
                } else {
                    this.logger.warn("All areas seems to be disabled (try " + attemptsCount + "/" + this.MAX_READ_ATTEMPTS + ") !");
                }
            }
            reject("All areas seems to be disabled !");
        });

    };

    public labels = () => {
        return new Promise<LabelsDto>((resolve) => {

            if (null == this.cacheLabels) {
                this.httpCall("/index.html").then((data: string) => {
                    const alarmName: string = data.split('top.document.title="')[1].split('";')[0];
                    const areaNames: Array<any> = JSON.parse('[' + data.split('tbl_areanam = new Array(')[1].split(');')[0] + ']');
                    const zoneNames: Array<any> = JSON.parse('[' + data.split('tbl_zone = new Array(')[1].split(');')[0] + ']');
                    const troubleNames: Array<any> = JSON.parse('[' + data.split('tbl_troublename = new Array(')[1].split(');')[0] + ']');

                    this.logger.debug(JSON.stringify(alarmName));
                    this.logger.debug(JSON.stringify(areaNames));
                    this.logger.debug(JSON.stringify(zoneNames));
                    this.logger.debug(JSON.stringify(troubleNames));

                    const labelsDto = new LabelsDto();
                    labelsDto.zoneNames = zoneNames;
                    labelsDto.alarmName = alarmName;
                    labelsDto.troubleNames = troubleNames;
                    labelsDto.areaNames = areaNames;

                    this.cacheLabels = labelsDto;

                    resolve(labelsDto);
                })
            } else {
                this.logger.debug("Use cached labels");
                resolve(this.cacheLabels);
            }
        });
    };

    public _ioStatus = (): Promise<Array<any>> => this.httpCall("io_sync.html?msgid=3&")
        .then((data: string) => new Promise<Array<IoCfgDto>>(resolve => {
            let ___ = {
                status: [
                    {
                        "mode": 0, //0->entree 1->sortie
                        "level": 1, // 1 si actif
                        "delayType": 0, // 0=> aucun delais en cours, 1-> decompte avant activation, 2 => duree activation restante
                        "hh": 0, "mm": 0, "ss": 0
                    }]
            };
            let ioData: any = JSON.parse(data);

            var ios: Array<IoCfgDto> = [];
            for (let ioIndex in ioData) {
                // ios.push(new IoCfgDto(Number(ioIndex) + 1, IoStatusLevel.fromCode(ioData[ioIndex].mode, IoStatusLevel)));
            }
            resolve(ios);
        }));

    public ioStatus = (): Promise<Array<IoCfgDto>> => this.httpCall("/io.html")
        .then((data: string) => new Promise<Array<IoCfgDto>>(resolve => {
            const ioCfg1: Array<any> = JSON.parse('[' + decodeURIComponent(data.split('io_cfg1 = new Array(')[1].split(');')[0].replace(/\\/g, '')) + ']');
            const ioCfg2: Array<any> = JSON.parse('[' + decodeURIComponent(data.split('io_cfg2 = new Array(')[1].split(');')[0].replace(/\\/g, '')) + ']');

            let ioCfgDtos: Array<IoCfgDto> = [];
            ioCfgDtos.push(new IoCfgDto(1, IoActive.fromCode(ioCfg1[12], IoActive), ioCfg1[14]));
            ioCfgDtos.push(new IoCfgDto(2, IoActive.fromCode(ioCfg2[12], IoActive), ioCfg2[14]));
            resolve(ioCfgDtos);
        }));

    public toggleIo = (ioId: number): Promise<any> =>
        this.httpCall(this.PAGE_IO.replace("{ioId}", "" + ioId).replace("{rnd}", "" + new Date().getTime()))
            .then(() => new Promise(resolve => resolve()));

    public events = (): Promise<Array<EventDto>> => this.httpCall(this.PAGE_EVENT)
        .then((data: string) => new Promise<Array<EventDto>>(resolve => {
            let es: Array<any> = JSON.parse(data.split('events =')[1].split(']')[0] + ']');
            let events: Array<EventDto> = [];
            for (let e of es) {
                console.log(e);
                let eventDto: EventDto = new EventDto();
                eventDto.date = new Date(e.time);
                eventDto.name = e.name;
                eventDto.status = e.status || null;

                for (let paramName in e.params) {
                    if (paramName && !e.params[paramName]) {
                        eventDto.detail = paramName;
                    } else if (e.params[paramName]) {
                        eventDto.params.push(e.params[paramName]);
                    }
                }
                events.push(eventDto);
            }
            resolve(events);
        }));

    public setAreaArmingMode = (area: Area, armingMode: ArmingMode) => {
        return this.wait(1)
            .then(() => this.httpCall(this.PAGE_SET_AREA_STATUS.replace("{area}", area.code).replace("{mode}", armingMode.code)))
            .then(() => this.wait(4000))
            .then(() => new Promise(resolve => resolve()))
    };

    private httpCall = (url: string) => new Promise<string>((resolve, reject) => setTimeout(() => {
            url = this.host + url;
            this.logger.debug(url + " - begin");
            this.connector.get(url, {rejectUnauthorized: false}, (res: any) => {
                const {statusCode} = res;
                let output: string = '';
                res.on('data', function (chunk: string) {
                    output += chunk;
                });
                res.on('end', () => {
                    this.logger.trace(output);
                    if (statusCode >= 200 && statusCode < 300) {
                        this.logger.debug(url + " - end");
                        resolve(output);
                    } else {
                        this.logger.error(url + " - end with error - status code : " + statusCode);
                        reject(output);
                    }
                });
            }).on('error', (e) => {
                this.logger.debug(url + " - end with error");
                this.logger.error(e.message);
                reject(e);
            })
        }, 10
    ));


    private wait = (delay: number, resolveValue: any = null) => new Promise((resolve) => setTimeout(() => resolve(resolveValue), delay));

}






