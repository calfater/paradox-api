import {SearchableByNameAndCode} from "./searchable-by-name-and-code";

export class Area extends SearchableByNameAndCode {
    static A1: Area = new Area(1, '00');
    static A2: Area = new Area(2, "01");
    static A3: Area = new Area(3, "02");
    static A4: Area = new Area(4, "03");
    static A5: Area = new Area(5, "04");
    static A6: Area = new Area(6, "05");
    static A7: Area = new Area(7, "06");
    static A8: Area = new Area(8, "07");
}
