import {SearchableByNameAndCode} from "./searchable-by-name-and-code";
import  {Description} from "@tsed/swagger";


export class ArmingMode extends SearchableByNameAndCode {
    static Regular: ArmingMode = new ArmingMode("regular", "r");
    static Forced: ArmingMode = new ArmingMode("forced", "f");
    static Partial: ArmingMode = new ArmingMode("partial", "p");
    static Immediate: ArmingMode = new ArmingMode("immediate", "i");
    static Ready: ArmingMode = new ArmingMode("ready", "d");
}
