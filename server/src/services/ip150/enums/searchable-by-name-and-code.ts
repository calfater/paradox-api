export abstract class SearchableByNameAndCode {

    constructor(public name: any, public code: string) {
    }

    public static fromName = (name: any, type: any) => {
        for (let p of Object.getOwnPropertyNames(type)) {
            if ("" + type[p].name === "" + name) {
                return type[p];
            }
        }
    };

    public static fromCode = (code: string, type: any) => {
        for (let p of Object.getOwnPropertyNames(type)) {
            if ("" + type[p].code == "" + code) {
                return type[p];
            }
        }
    };

}
