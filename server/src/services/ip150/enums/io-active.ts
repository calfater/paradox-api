import {SearchableByNameAndCode} from "./searchable-by-name-and-code";

export class IoActive extends SearchableByNameAndCode {
    static Yes: IoActive = new IoActive(true, "1");
    static No: IoActive = new IoActive(false, "0");
}
