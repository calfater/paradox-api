import {SearchableByNameAndCode} from "./searchable-by-name-and-code";

export class ZoneStatus extends SearchableByNameAndCode {
    static Closed: ZoneStatus = new ZoneStatus('closed', "0");
}
