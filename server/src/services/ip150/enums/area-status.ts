import {SearchableByNameAndCode} from "./searchable-by-name-and-code";

export class AreaStatus extends SearchableByNameAndCode {
    static Disabled: AreaStatus = new AreaStatus('disabled', "0");
    static Armed: AreaStatus = new AreaStatus('armed', "2");
    static Partial: AreaStatus = new AreaStatus('partial', "5");
    static Immediate: AreaStatus = new AreaStatus('immediate', "10");
    static Ready: AreaStatus = new AreaStatus('ready', "8");
    static LeStatus9: AreaStatus = new AreaStatus('le_status_9', "9");
}
