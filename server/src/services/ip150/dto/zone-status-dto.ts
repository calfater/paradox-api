import {ZoneStatus} from "../enums/zone-status";

export class ZoneStatusDto {
    public label: string;

    constructor(public zone: number, public status: ZoneStatus) {
    }

}
