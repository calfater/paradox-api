import {IoActive} from "../enums/io-active";

export class IoCfgDto {
    constructor(public id: number, public active: IoActive, public label: string) {

    }

}

