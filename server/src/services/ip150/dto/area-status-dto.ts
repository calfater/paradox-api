import {Area} from "../enums/area";
import {AreaStatus} from "../enums/area-status";

export class AreaStatusDto {
    public label: string;

    constructor(public area: Area, public status: AreaStatus) {
    }

}
