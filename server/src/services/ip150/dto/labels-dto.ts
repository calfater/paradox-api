export class LabelsDto {

    public alarmName: string;
    public areaNames: Array<any>;
    public zoneNames: Array<any>;
    public troubleNames: Array<any>;

}