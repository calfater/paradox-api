import {AreaStatusDto} from "./area-status-dto";
import {ZoneStatusDto} from "./zone-status-dto";
import {AlarmeStatusDto} from "./alarme-status-dto";
import {TroubleStatusDto} from "./trouble-status-dto";

export class StatusLiveDto {
    public areas: Array<AreaStatusDto>;
    public zones: Array<ZoneStatusDto>;
    public alarms: Array<AlarmeStatusDto>;
    public troubles: Array<TroubleStatusDto>;

    constructor() {

    }


}