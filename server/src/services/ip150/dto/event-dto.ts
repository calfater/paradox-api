export class EventDto {
    public date: Date;
    public name: string;
    public status: string;
    public detail: string;
    public params: Array<String> = [];

    constructor() {
    }

}
