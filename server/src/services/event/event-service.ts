import {Service} from "@tsed/di";
import {Ip150Service} from "../ip150/ip150-service";
import {IoActive} from "../ip150/enums/io-active";

@Service()
export class EventService {

    constructor(private ip150Service: Ip150Service) {
    }

    public getEvents = () => {
        return this.ip150Service.authenticate()
            .then(this.ip150Service.events)
            .finally(this.ip150Service.logout);
    };

}