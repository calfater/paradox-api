import {Ip150Service} from "../ip150/ip150-service";
import {ZoneStatusDto} from "../ip150/dto/zone-status-dto";
import {Service} from "@tsed/di";
import {LabelsDto} from "../ip150/dto/labels-dto";

@Service()
export class ZoneService {

    constructor(private ip150Service: Ip150Service) {

    }

    public getZonesState = () => {
        let zones: Array<ZoneStatusDto> = [];

        return this.ip150Service.authenticate()
            .then(this.ip150Service.statusLive)
            .then((status: any) => new Promise<Array<ZoneStatusDto>>(resolve => {
                zones = status.zones;
                resolve();
            }))
            .then(this.ip150Service.labels)
            .then((labels: LabelsDto) => new Promise(resolve => {
                for (let statusIdx in zones) {
                    zones[statusIdx].label = labels.zoneNames[statusIdx];
                }
                resolve();
            }))
            .then(() => new Promise<Array<ZoneStatusDto>>(resolve => {
                let zonesTrimed: Array<ZoneStatusDto> = [];
                for (let zone of zones) {
                    if (typeof zone.label == 'string' && zone.label.trim() !== "") {
                        zonesTrimed.push(zone);
                    }
                }
                resolve(zonesTrimed);
            }))
            .finally(this.ip150Service.logout);
    }
}