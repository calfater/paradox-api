import * as path from 'path';

export class LoggerService {

    public static level: number = 2;

    public static readonly Level: any = {
        None: 0,
        Warn: 1,
        Info: 2,
        Debug: 3,
        Trace: 4,
    };

    public constructor(private readonly name: string) {
        this.name = name.replace(path.join(__dirname, "../../"), "");
    }

    error = (msg: any) => {
        this.log('ERROR', msg);
    };


    warn = (msg: any) => {
        if (LoggerService.level >= 1) {
            this.log('WARN ', msg);
        }
    };

    info = (msg: any) => {
        if (LoggerService.level >= 2) {
            this.log('INFO ', msg);
        }
    };

    debug = (msg: any) => {
        if (LoggerService.level >= 3) {
            this.log('DEBUG', msg);
        }
    };

    trace = (msg: any) => {
        if (LoggerService.level >= 4) {
            this.log('TRACE', msg);
        }
    };

    private log = (level: string, msg: any) => {
        console.log(`[${level}] - ${new Date().toLocaleString()} - ${this.name} - `, msg);
    };

}
