import {Service} from "@tsed/di";
import {Ip150Service} from "../ip150/ip150-service";
import {ArmingMode} from "../ip150/enums/arming-mode";
import {StatusLiveDto} from "../ip150/dto/status-live-dto";
import {Area} from "../ip150/enums/area";
import {AreaStatusDto} from "../ip150/dto/area-status-dto";
import {LoggerService} from "../logger/logger-service";
import {LabelsDto} from "../ip150/dto/labels-dto";

@Service()
export class AreaService {

    private logger: LoggerService = new LoggerService(__filename);

    constructor(private ip150Service: Ip150Service) {
    }

    public getAreas = () => {
        let areas: Array<AreaStatusDto> = [];

        return this.ip150Service.authenticate()
            .then(this.ip150Service.statusLive)
            .then((status: StatusLiveDto) => this.extractAreas(areas, status))
            .then(this.ip150Service.labels)
            .then((labels: LabelsDto) => this.fillLabels(areas, labels.areaNames))
            .then(() => new Promise<Array<AreaStatusDto>>(resolve => resolve(areas)))
            .finally(this.ip150Service.logout);
    };

    public getArea = (area: any) => {
        let areas: Array<AreaStatusDto> = [];

        return this.ip150Service.authenticate()
            .then(this.ip150Service.statusLive)
            .then((status: StatusLiveDto) => this.extractAreas(areas, status))
            .then(this.ip150Service.labels)
            .then((labels: LabelsDto) => this.fillLabels(areas, labels.areaNames))
            .then(() => new Promise<any>((resolve, reject) => {
                for (let areaStatusDto of areas) {
                    if (area.name === areaStatusDto.area.name) {
                        resolve(areaStatusDto)
                    }
                }
                this.logger.error("Area " + area + " not found");
                reject(null);
            }))
            .finally(this.ip150Service.logout);
    };

    public setAreaArmingMode = (area: Area, armingMode: ArmingMode) => this.ip150Service.authenticate()
        .then(() => this.ip150Service.setAreaArmingMode(area, armingMode))
        .finally(this.ip150Service.logout);


    private extractAreas(areas: Array<any>, status: StatusLiveDto) {
        return new Promise(resolve => {
            Array.prototype.push.apply(areas, status.areas);
            resolve();
        });
    }

    private fillLabels = (areas: Array<AreaStatusDto>, labels: Array<string>) =>
        new Promise(resolve => {
            for (let areaIdx in areas) {
                areas[areaIdx].label = labels[areaIdx];
            }
            resolve();
        });


}