import {Service} from "@tsed/di";
import {Ip150Service} from "../ip150/ip150-service";
import {IoActive} from "../ip150/enums/io-active";

@Service()
export class IoService {

    constructor(private ip150Service: Ip150Service) {
    }

    public getIos = () => {
        return this.ip150Service.authenticate()
            .then(this.ip150Service.ioStatus)
            .finally(this.ip150Service.logout);
    };

    public getIo = (id: number) => {
        return this.ip150Service.authenticate()
            .then(this.ip150Service.ioStatus)
            .then((status) => new Promise((resolve, reject) => 1 == id || 2 == id ? resolve(status[id - 1]) : reject(`Invalid IO id : ${id}`)))
            .finally(this.ip150Service.logout);
    };

    public toggleIo = (id: number) => {
        return this.ip150Service.authenticate()
            .then(() => this.ip150Service.toggleIo(id))
            .finally(this.ip150Service.logout);
    };


}