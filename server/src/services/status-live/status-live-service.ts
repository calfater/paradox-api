import {Ip150Service} from "../ip150/ip150-service";
import {Inject, Service} from "@tsed/di";

@Service()
export class StatusLiveService {

    constructor(private ip150Service: Ip150Service) {

    }

    public getStatusLive = () =>
        this.ip150Service.authenticate()
            .then(this.ip150Service.statusLive)
            .then(this.ip150Service.logout)
            .catch(this.ip150Service.logout);

}