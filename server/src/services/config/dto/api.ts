import {User} from "./user";

export class Api {
    attemptRead: number;
    attemptWrite: number;
    attemptCheck: number;
    forwardCredentials: boolean;
    users: Array<User>;
}
