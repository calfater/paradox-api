import {Api} from "./api";
import {IpModule} from "./ip-module";

export class Config {
    logger: string;
    ipModule: IpModule;
    api: Api;
}
