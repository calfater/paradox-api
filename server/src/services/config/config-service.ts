import {Config} from "./dto/config";
import * as fs from 'fs';
import * as path from 'path';
import * as os from 'os';
import {LoggerService} from "../logger/logger-service";
import {Service} from "@tsed/di";

@Service()
export class ConfigService {

    public config: Config = JSON.parse(fs.readFileSync(path.join(__dirname, "../../config-default.json")).toString());

    private userConfigFile: string = path.join(__dirname, "../../config.json");
    private userConfigFileHome: string = path.join(os.homedir(), ".paradox-api.json");
    private loggerService = new LoggerService(__filename);

    constructor() {
        let userConfig: Config = new Config();
        if (fs.existsSync(this.userConfigFile)) {
            this.loggerService.info(`Use config : ${this.userConfigFile}`);
            userConfig = JSON.parse(fs.readFileSync(this.userConfigFile).toString())
        }
        if (fs.existsSync(this.userConfigFileHome)) {
            this.loggerService.info(`Use config : ${this.userConfigFileHome}`);
            userConfig = JSON.parse(fs.readFileSync(this.userConfigFileHome).toString())
        }

        // this.config.ipModule =
        if (userConfig.logger) this.config.logger = userConfig.logger;
        if (userConfig.ipModule && userConfig.ipModule.url) this.config.ipModule.url = userConfig.ipModule.url;
        if (userConfig.ipModule && userConfig.ipModule.username) this.config.ipModule.username = userConfig.ipModule.username;
        if (userConfig.ipModule && userConfig.ipModule.password) this.config.ipModule.password = userConfig.ipModule.password;
        if (userConfig.api && userConfig.api.users && userConfig.api.users.length > 0) {
            this.config.api.users = userConfig.api.users;
        }

    }

}

