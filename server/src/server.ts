import {$log, GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings} from "@tsed/common";
import {LoggerService} from "./services/logger/logger-service";
import {Docs} from "@tsed/swagger";
import {BasicAuthMiddleware} from "./middlewares/basic-auth-middleware";
import {NotFoundMiddleware} from "./middlewares/not-found-middleware";

const bodyParser = require("body-parser");
// const cookieParser = require("cookie-parser");
// const compress = require("compression");
// const methodOverride = require("method-override");

@ServerSettings({
    rootDir: __dirname,
    statics: {
        "/": `${__dirname}/webapp/`
    },
    acceptMimes: ["application/json"],
    componentsScan: [
        "${rootDir}/middlewares/**/*.ts",
        "${rootDir}/controllers/converters/**/*.ts",
        "${rootDir}/converters/**/*.ts",
        "${rootDir}/services/**/*.ts",
        "${rootDir}/filters/**/*.ts",
    ],
    mount: {
        "/api": "${rootDir}/controllers/**/*.ts"
    },
    port: 3000,
    swagger: [
        {
            path: "/api"
        }
    ],
    logger: {
        level: "info"
    }
})

@Docs()
export class Server extends ServerLoader {

    constructor() {
        super();
        LoggerService.level = LoggerService.Level.Debug;
    }

    public $beforeRoutesInit(): void | Promise<any> {
        this
            .use(GlobalAcceptMimesMiddleware)
            .use(BasicAuthMiddleware)
            // .use(JwtAuthMiddleware)
            // .use(cookieParser())
            // .use(compress({}))
            // .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
                extended: true
            }));
    }

    public $afterRoutesInit() {
        this.use(NotFoundMiddleware);
    }
}

async function bootstrap() {
    try {
        $log.debug("Start server...");
        const server = await Server.bootstrap(Server);
        await server.listen();
        $log.debug("Server initialized");
    } catch (er) {
        $log.error(er);
    }
}

bootstrap();