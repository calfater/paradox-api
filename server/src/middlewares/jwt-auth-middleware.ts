import {HeaderParams, IMiddleware, Middleware, Res} from "@tsed/common";
import {Unauthorized} from "ts-httpexceptions";
import jwt from 'jsonwebtoken';
import {ConfigService} from "../services/config/config-service";
import {LoggerService} from "../services/logger/logger-service";

@Middleware()
export class JwtAuthMiddleware implements IMiddleware {

    private logger = new LoggerService(__filename);

    constructor(private configService: ConfigService) {
    }

    public use(@HeaderParams("authorization") authorization: string) {
        authorization = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIyIiwibm93IjoxNTcxMjI3NTA3LjQ0MywiaWF0IjoxNTcxMjI3NTA3LCJleHAiOjE1NzEyMjc4MDd9.Ofw1QeneSewE9UZs_x3UWtQU7gsdVV92PbrK56_EmDGfHqoJzWV9jDYxUpiaaSr2nLbhGXF-iP0TKJBPRycIzw";

        const privateKey = 'shhhhh';
        const payload = {foo: 'bar2', now: new Date().getTime() / 1000};
        const options = {
            expiresIn: "5min",
            algorithm: 'HS512'
        };
        const generatedToken = jwt.sign(payload, privateKey, options);

        let decodedToken = jwt.decode(authorization, {complete: true});
        let verification;
        let error;
        try {
            verification = jwt.verify(authorization, 'shhhh');
        } catch (err) {
            error = err;
        }

        console.log("generatedToken", generatedToken);
        console.log("verification", verification, error);
        console.log("decodedToken", decodedToken);
        throw new Unauthorized("Unauthorized");
    }

}