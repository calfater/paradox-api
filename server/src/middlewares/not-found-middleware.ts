import {IMiddleware, Middleware, Res} from "@tsed/common";
import * as Express from "express";

@Middleware()
export class NotFoundMiddleware implements IMiddleware {

    public use(@Res() response: Express.Response) {
        response.redirect(301, "/");
        response.end();
    }

}