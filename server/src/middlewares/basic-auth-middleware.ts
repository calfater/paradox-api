import {HeaderParams, IMiddleware, Middleware, Res} from "@tsed/common";
import {Unauthorized} from "ts-httpexceptions";
import {ConfigService} from "../services/config/config-service";
import {LoggerService} from "../services/logger/logger-service";

@Middleware()
export class BasicAuthMiddleware implements IMiddleware {

    private static HEADER_WWW_AUTHENTICATE = "WWW-Authenticate";
    private static HEADER_WWW_AUTHENTICATE_VALUE = 'Basic realm="{msg}"';

    private logger = new LoggerService(__filename);

    constructor(private configService: ConfigService) {
    }

    public use(@Res() res: Res, @HeaderParams("authorization") authorization: string) {
        let username: string;
        let password: string;
        let authenticated: boolean;

        if (authorization) {
            let h: any = Buffer.from(authorization.split(" ")[1], 'base64').toString().split(":");
            username = h[0];
            password = h[1];
        }

        authenticated = (this.configService.config.api.users[0].username === username && this.configService.config.api.users[0].password === password);

        if (authenticated) {
            this.logger.debug("User " + username + " : authenticated");
        } else {
            this.logger.warn("User " + username + " : access denied !");
            res.setHeader(BasicAuthMiddleware.HEADER_WWW_AUTHENTICATE, BasicAuthMiddleware.HEADER_WWW_AUTHENTICATE_VALUE.replace('{msg}', "Paradox API : authentication required"));
            throw new Unauthorized("Unauthorized");
        }
    }

}